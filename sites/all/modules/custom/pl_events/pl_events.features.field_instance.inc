<?php
/**
 * @file
 * pl_events.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function pl_events_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-event-field_event_ongoing'
  $field_instances['node-event-field_event_ongoing'] = array(
    'bundle' => 'event',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_event_ongoing',
    'label' => 'Ongoing',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Ongoing');

  return $field_instances;
}
