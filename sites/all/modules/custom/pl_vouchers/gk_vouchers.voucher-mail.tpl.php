<div class="voucher">
  <table class="row prose voucher__intro">
    <tr>
      <td>
        <?php echo $intro ?>
      </td>
    </tr>
  </table>

  <table class="row prose voucher__offer">
    <tr>
      <td>
        <?php echo $offer ?>*
      </td>
    </tr>
  </table>

  <table class="row prose voucher__summary">
    <tr>
      <td>
        <?php echo $summary ?>
      </td>
    </tr>
  </table>

  <table class="row prose voucher__code">
    <tr>
      <td>
        Voucher code: <?php echo $voucher_code ?>
      </td>
    </tr>
  </table>

  <?php if (!empty($qr_code)): ?>
    <table class="row prose voucher__qr-code">
      <tr>
        <td>
          <?php echo $qr_code ?>
        </td>
      </tr>
    </table>
  <?php endif; ?>

  <?php if (!empty($ctas)): ?>
    <?php foreach ($ctas as $cta): ?>
      <table class="row prose voucher__cta">
        <tr>
          <td class="center">

            <table class="medium-button">
              <tr>
                <td>
                  <a href="<?php echo $cta['url'] ?>"><?php echo $cta['title'] ?></a>
                </td>
              </tr>
            </table>

          </td>
        </tr>
      </table>
    <?php endforeach; ?>
  <?php endif; ?>

  <table class="row prose voucher__terms">
    <tr>
      <td>
        <strong>* Terms and Conditions:</strong>
        <?php echo $terms_and_conditions ?>
      </td>
    </tr>
  </table>

  <table class="row prose voucher__date-sent">
    <tr>
      <td>
        <?php echo $date_sent ?>
      </td>
    </tr>
  </table>
</div>
