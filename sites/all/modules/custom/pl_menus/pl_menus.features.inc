<?php
/**
 * @file
 * pl_menus.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pl_menus_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
}
