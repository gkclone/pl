<?php

$plugin = array(
  'title' => 'Menus: Banner image',
  'category' => 'Premium Locals',
  'single' => TRUE,
);

function pl_menus_banner_image_content_type_render($subtype, $conf, $args, $context) {
  if (($gk_menu = menu_get_object('gk_menu')) && $items = field_get_items('gk_menu', $gk_menu, 'field_menu_banner_image')) {
    return (object) array(
      'title' => '',
      'content' => theme('image', array(
        'path' => file_create_url($items[0]['uri']),
      )),
    );
  }
}
