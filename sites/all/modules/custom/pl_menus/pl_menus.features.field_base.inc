<?php
/**
 * @file
 * pl_menus.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function pl_menus_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_menu_banner_image'
  $field_bases['field_menu_banner_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_menu_banner_image',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'entity_translation_sync' => array(
        0 => 'fid',
      ),
      'uri_scheme' => 'public',
    ),
    'translatable' => 1,
    'type' => 'image',
  );

  return $field_bases;
}
