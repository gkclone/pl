<?php

// Plugin definition
$plugin = array(
  'title' => t('Premium Locals page template: Primary'),
  'category' => t('Premium Locals'),
  'icon' => 'pl_core_page_primary.png',
  'theme' => 'minima_page_layout',
  'regions' => array(
    'primary_first' => t('Primary first'),
    'primary_second' => t('Primary second'),
    'secondary' => t('Secondary'),
    'tertiary_first' => t('Tertiary first'),
    'tertiary_second' => t('Tertiary second'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_pl_core_page_primary($variables) {
  return array(
    'primary' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'div',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--primary'),
      ),
      'primary_first' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['primary_first'],
      ),
      'primary_second' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['primary_second'],
      ),
    ),
    'secondary' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--secondary'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--space'),
      ),
      'secondary' => array(
        '#markup' => $variables['content']['secondary'],
      ),
    ),
    'tertiary' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'div',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--tertiary'),
      ),
      'tertiary_first' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['tertiary_first'],
      ),
      'tertiary_second' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['tertiary_second'],
      ),
    ),
  );
}
