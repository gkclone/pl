<?php

$plugin = array(
  'title' => 'PL: Page Top Link',
  'category' => 'Premium Locals',
  'single' => TRUE,
);

function pl_core_page_top_link_content_type_render($subtype, $conf, $args, $context) {
  return (object) array(
    'title' => '',
    'content' => array(
      'page_top_link' => array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array(
            'PageTopLink',
          ),
        ),
        '#markup' => l(t('Back to top'), '', array(
          'fragment' => ' ',
          'external' => true,
        )),
      ),
    ),
  );
}
