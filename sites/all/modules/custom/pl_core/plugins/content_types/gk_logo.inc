<?php

$plugin = array(
  'title' => 'PL: GK Logo',
  'category' => 'Premium Locals',
  'single' => TRUE,
);

function pl_core_gk_logo_content_type_render($subtype, $conf, $args, $context) {
  global $theme;
  $minima_settings = theme_get_setting('minima_config');
  $colour_scheme = $minima_settings['settings']['colour_scheme'];
  $gk_logo = libraries_get_path('style_guide_' . $theme) . '/src/images/' . $colour_scheme . '/gk-logo.png';
  if (file_exists($gk_logo)) {
    return (object) array(
      'title' => '',
      'content' => theme('image', array(
        'path' => $gk_logo,
      )),
    );
  }
}
