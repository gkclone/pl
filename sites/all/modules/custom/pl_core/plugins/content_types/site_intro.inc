<?php

$plugin = array(
  'title' => 'PL: Site intro',
  'category' => 'Premium Locals',
  'single' => TRUE,
);

function pl_core_site_intro_content_type_render($subtype, $conf, $args, $context) {
  $content = array(
    'slogan' => array(
      '#theme_wrappers' => array('container'),
      '#attributes' => array(
        'class' => array(
          'SiteIntro',
        ),
      ),
      'content' => array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array(
            'SiteIntro-slogan',
          ),
        ),
        '#markup' => variable_get('site_slogan'),
      ),
      'cta' => array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array(
            'SiteIntro-cta',
            'u-textCenter'
          ),
        ),
        '#markup' => l('Get to know us. You\'ll like us, we promise', 'find-us',
          array('attributes' => array(
            'class' => array('Button'),
          ))
        ),
      ),
    ),
    'signup_cta_form' => drupal_get_form('gk_members_signup_cta_form'),
  );

  if ($current_location = gk_locations_get_current_location()) {
    $body = field_get_items('node', $current_location, 'body');
    $description = array_shift($body);

    $content['slogan']['site_description'] = array(
      '#theme_wrappers' => array('container'),
      '#attributes' => array(
        'class' => array(
          'SiteIntro-description',
        ),
        'style' => array(
          'display: none;',
        ),
      ),
      '#markup' => $description['value'] . l('Flip back', '',
        array('attributes' => array(
          'class' => array('Button'),
        ))
      ),
    );
  }

  return (object) array(
    'title' => '',
    'content' => $content,
  );
}
