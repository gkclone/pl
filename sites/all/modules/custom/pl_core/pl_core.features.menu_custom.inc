<?php
/**
 * @file
 * pl_core.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function pl_core_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-footer-secondary-menu.
  $menus['menu-footer-secondary-menu'] = array(
    'menu_name' => 'menu-footer-secondary-menu',
    'title' => 'Footer secondary menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Footer secondary menu');


  return $menus;
}
