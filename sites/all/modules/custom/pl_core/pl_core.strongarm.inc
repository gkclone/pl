<?php
/**
 * @file
 * pl_core.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pl_core_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_access_page';
  $strongarm->value = 1;
  $export['gk_access_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_copyright';
  $strongarm->value = '&copy; Copyright Greene King PLC';
  $export['site_copyright'] = $strongarm;

  return $export;
}
