<?php
/**
 * @file
 * pl_translation.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pl_translation_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_translation_entity_types';
  $strongarm->value = array(
    'promotion' => 'promotion',
    'promotion_group' => 'promotion_group',
    'reward' => 0,
    'node' => 0,
    'file' => 0,
    'taxonomy_term' => 0,
    'user' => 0,
  );
  $export['entity_translation_entity_types'] = $strongarm;

  return $export;
}
