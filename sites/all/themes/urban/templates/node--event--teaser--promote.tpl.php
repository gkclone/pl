<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <div<?php print $content_attributes; ?>>
      <div class="Grid-cell">
        <?php print render($field_event_image); ?>
        <?php if ($display_title): ?>
          <h2 class="Node-title">
            <?php print $title; ?>
          </h2>
        <?php endif; ?>
      </div>
      <div class="Grid-cell">
        <?php print render($field_event_date); ?>
        <?php print $body; ?>
        <?php if (!empty($links)): ?>
          <?php print $links; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php endif; ?>
