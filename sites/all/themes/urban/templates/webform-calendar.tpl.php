<?php

/**
 * @file
 * Theme the button for the date component date popup.
 */
?>
<?php
 $idKey = str_replace('_', '-', $component['form_key']);
?>
<input type="text" id="edit-submitted-<?php print $idKey ?>" class="is-hiddenVisually form-text <?php print implode(' ', $calendar_classes); ?>" alt="<?php print t('Open popup calendar'); ?>" title="<?php print t('Open popup calendar'); ?>" placeholder="<?php print t('Pick a date'); ?>" />
