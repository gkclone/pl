<?php

/**
 * Implements hook_minima_config_settings().
 */
function urban_minima_config_settings() {
  return array(
    'colour_scheme' => array(
      'title' => 'Colour Scheme',
      'options' => array(
        'dark' => array(
          'title' => 'Dark',
          'variables' => array(
            'minima-config-variation' => 'dark',
          ),
        ),
        'light' => array(
          'title' => 'Light',
          'variables' => array(
            'minima-config-variation' => 'light',
          ),
        ),
      ),
    ),
  );
}

/**
 * Preprocess variables for theme panels_pane.
 */
function urban_preprocess_panels_pane(&$variables) {
  $type = $variables['pane']->type;
  $subtype = $variables['pane']->subtype;

  // Location info boxes.
  if ($type == 'gk_locations_info' && $subtype == 'gk_locations_info') {
    $variables['attributes_array']['class'][] = 'Box--primary';
  }
  // Promoted event box.
  elseif ($type == 'gk_events_list' && $subtype == 'gk_events_list' && $variables['is_front']) {
    $variables['attributes_array']['class'][] = 'Box--secondary';
  }
  // Event categories box.
  elseif ($type == 'gk_events_categories' && $subtype == 'gk_events_categories') {
    $variables['attributes_array']['class'][] = 'Box--primary';
  }
  elseif ($type == 'block' && $subtype == 'menu_block-gk-members-user-menu') {
    $variables['attributes_array']['class'][] = 'Box--primary';
    $variables['title'] = t('Navigation');
  }
}

/**
 * Preprocess variables for theme minima_page_layout.
 */
function urban_preprocess_minima_page_layout(&$variables) {
  if ($variables['layout']['name'] == 'pl_core_page_primary') {
    // Adding grid classes to the primary section
    $variables['minima_layout']['primary']['#grid_attributes'] =  array(
      'class' => array('Grid--space'),
    );
    $variables['minima_layout']['primary']['primary_first']['#grid_cell_attributes'] = array(
      'class' => array('u-xl-size2of3', 'u-ie-size2of3'),
    );
    $variables['minima_layout']['primary']['primary_second']['#grid_cell_attributes'] = array(
      'class' => array('u-xl-size1of3', 'u-ie-size1of3'),
    );

    // Adding grid classes to the tertiary section
    $variables['minima_layout']['tertiary']['#grid_attributes'] =  array(
      'class' => array('Grid--space'),
    );
    $variables['minima_layout']['tertiary']['tertiary_first']['#grid_cell_attributes'] = array(
      'class' => array('u-xl-size1of2', 'u-ie-size1of2'),
    );
    $variables['minima_layout']['tertiary']['tertiary_second']['#grid_cell_attributes'] = array(
      'class' => array('u-xl-size1of2', 'u-ie-size1of2'),
    );
  }
}