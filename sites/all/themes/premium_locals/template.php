<?php

/**
 * Implementation of hook_theme_registry_alter().
 */
function premium_locals_theme_registry_alter(&$theme_registry) {
  $theme_registry['panels_pane']['path'] = drupal_get_path('theme', 'premium_locals') . '/templates';
}

/**
 * Implements hook_html_head_alter().
 */
function premium_locals_html_head_alter(&$head_elements) {
  global $theme;

  // Unset any already assigned favicon
  foreach ($head_elements as $key => $head_element) {
    if (isset($head_element['#attributes']['rel']) && $head_element['#attributes']['rel'] == 'shortcut icon') {
      unset($head_elements[$key]);
    }
  }

  // Include the theme/colour scheme's favicon
  $minima_settings = theme_get_setting('minima_config');
  $colour_scheme = $minima_settings['settings']['colour_scheme'];

  $style_guide_path = libraries_get_path('style_guide_' . $theme);
  $favicon_path = '/' . $style_guide_path . '/src/images/' . $colour_scheme . '/favicon.ico';

  $head_elements['link_favicon'] = array(
    '#type' => 'html_tag',
    '#tag' => 'link',
    '#attributes' => array(
      'rel' => 'shortcut icon',
      'href' => $favicon_path,
      'type' => 'image/x-icon',
    ),
  );
}

/**
 * Implements hook_form_alter().
 */
function premium_locals_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#node']) && $form['#node']->type == 'webform') {
    $machine_name = gk_machine_name_lookup_by_entity($form['#node']->nid);

    // Alter the markup of the general enquiry webform to achieve a grid layout.
    if ($machine_name == 'pl_webform_general_enquiry') {
      $form['submitted']['#theme_wrappers'] = array('minima_grid');
      $form['submitted']['#grid_attributes'] = array(
        'class' => array('Grid--spaceHorizontal'),
      );

      $form['submitted']['first_name']['#prefix'] = '<div class="Grid-cell u-xl-size1of2 u-lg-size1of2">';
      $form['submitted']['email']['#suffix'] = '</div>';

      $form['submitted']['question']['#prefix'] = '<div class="Grid-cell u-xl-size1of2 u-lg-size1of2">';
      $form['submitted']['question']['#suffix'] = '</div>';

      $form['actions']['#attributes']['class'][] = 'u-textCenter';
    }

    // Hide the 'recipient email' field on all webforms.
    if (isset($form['submitted']['recipient_email'])) {
      $form['submitted']['recipient_email']['#grid_attributes']['class'] = array('is-hiddenVisually');
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function premium_locals_form_gk_members_signup_cta_form_alter(&$form, &$form_state, $form_id) {
  // Turn this form into a grid.
  $form['#attributes']['class'][] = 'Grid';
  $form['#attributes']['class'][] = 'Form--membersSignUp';

  $form['mail']['#prefix'] = '<div class="Grid-cell u-size5of6">';
  $form['mail']['#suffix'] = '</div>';
  $form['mail']['#attributes']['placeholder'] = t('Enter your email for the VIP treatment');

  $form['submit']['#value'] = '>>';
  $form['submit']['#attributes']['class'][] = 'Grid-cell';
  $form['submit']['#attributes']['class'][] = 'u-size1of6';

  // Add a title element.
  $form['title'] = array(
    '#theme' => 'html_tag',
    '#tag' => 'div',
    '#value' => 'Become our friends',
    '#weight' => -10,
    '#attributes' => array(
      'class' => array(
        'Form-title',
      ),
    ),
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function premium_locals_form_gk_rewards_user_details_form_alter(&$form, &$form_state, $form_id) {
  // Rearrange name fields into a grid...
  $form['name']['#theme_wrappers'] = array('minima_grid');
  $form['name']['#grid_attributes']['class'] = array('Grid--spaceHorizontal', 'Grid--formItemName');
  unset($form['name']['#type']);

  // ...name fields.
  $form['name']['fields'] = array(
    'label' => $form['name']['label'],
    'title' => $form['name']['title'],
    'first_name' => $form['name']['first_name'],
    'last_name' => $form['name']['last_name'],
  );

  unset($form['name']['label']);
  unset($form['name']['title']);
  unset($form['name']['first_name']);
  unset($form['name']['last_name']);

  // ...name label.
  $form['name']['fields']['label']['#type'] = 'item';
  $form['name']['fields']['label']['#title'] = t('Name');
  $form['name']['fields']['label']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['label']['#grid_cell_attributes']['class'] = array('u-size1of8', 'u-xs-sizeFull', 'u-sm-sizeFull');
  unset($form['name']['fields']['label']['#prefix']);

  // ...title.
  $form['name']['fields']['title']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['title']['#grid_cell_attributes']['class'] = array('u-size4of16', 'u-xs-sizeFull', 'u-sm-sizeFull');
  $form['name']['fields']['title']['#attributes']['required'] = TRUE;

  // ...first name.
  $form['name']['fields']['first_name']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['first_name']['#grid_cell_attributes']['class'] = array('u-size5of16', 'u-xs-sizeFull', 'u-sm-sizeFull');
  $form['name']['fields']['first_name']['#attributes']['required'] = TRUE;

  // ...last name.
  $form['name']['fields']['last_name']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['last_name']['#grid_cell_attributes']['class'] = array('u-size5of16', 'u-xs-sizeFull', 'u-sm-sizeFull');
  $form['name']['fields']['last_name']['#attributes']['required'] = TRUE;

  // Set grid cell classes and attributes for other fields...
  $size_label = array('u-size1of8', 'u-md-size1of5', 'u-lg-size1of6', 'u-xs-sizeFull', 'u-sm-sizeFull');
  $size_field = array('u-size7of8', 'u-md-size4of5', 'u-lg-size5of6', 'u-xs-sizeFull', 'u-sm-sizeFull');

  // ...email.
  $form['mail']['#title_grid_cell_attributes']['class'] = $size_label;
  $form['mail']['#field_grid_cell_attributes']['class'] = $size_field;
  $form['mail']['#attributes']['placeholder'] = 'E.g. yourname@example.com';
  $form['mail']['#attributes']['required'] = TRUE;
  unset($form['confirm_mail']);

  // ...postcode.
  $form['postcode']['#title_grid_cell_attributes']['class'] = $size_label;
  $form['postcode']['#field_grid_cell_attributes']['class'] = $size_field;
  $form['postcode']['#attributes']['placeholder'] = 'E.g. IP33 1QT';
  $form['postcode']['#attributes']['required'] = TRUE;
  $form['postcode']['#attributes']['pattern'] = '[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]? ?[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}';

  // ...date of birth.
  $form['date_of_birth']['#title_grid_cell_attributes']['class'] = $size_label;
  $form['date_of_birth']['#field_grid_cell_attributes']['class'] = $size_field;

  // ...communications opt-in.
  $form['comms_opt_in_other']['#minima_grid'] = TRUE;
  $form['comms_opt_in_other']['#title_grid_cell_attributes']['class'] = array('u-size9of10');
  $form['comms_opt_in_other']['#field_grid_cell_attributes']['class'] = array('u-size1of10');
}

/**
 * Implements hook_gk_menus_build_sections_alter().
 */
function premium_locals_gk_menus_build_sections_alter(&$build, $depth) {
  // Only apply grid cell classes if this is a top level section
  if ($depth == 0) {
    foreach ($build as &$section) {
      if (!empty($section['inner']['items'])) {
        $section['inner']['items']['#theme_wrappers'] = array('minima_grid');

        $section['inner']['items']['#grid_attributes'] = array(
          'class' => array(
            'Grid--space',
            'Box-content',
          ),
        );

        foreach (element_children($section['inner']['items']) as $miid) {
          $section['inner']['items'][$miid]['#theme_wrappers'] = array('minima_grid_cell');

          $section['inner']['items'][$miid]['#grid_cell_attributes'] = array(
            'class' => array(
              'u-xl-size1of2',
              'u-lg-size1of2',
            ),
          );
        }
      }
    }
  }
}

/**
 * Preprocess variables for the html template.
 */
function premium_locals_preprocess_html(&$variables) {
  // Override the language attribute since we're using Drupal's language system
  // to translate content based on domain, not language.
  $variables['html_attributes_array']['lang'] = 'en';

  // Add JS to the page.
  drupal_add_js(libraries_get_path('minima') . '/dist/js/minima.offcanvas.min.js');
  drupal_add_js(libraries_get_path('imagesloaded') . '/imagesloaded.pkgd.min.js');
  drupal_add_js(libraries_get_path('masonry') . '/dist/masonry.pkgd.min.js');
}

/**
 * Process variables for the html template.
 */
function premium_locals_process_html(&$variables, $hook) {
  // Add CSS/JS to the page.
  global $theme;

  if ($theme == 'premium_locals') {
    drupal_add_css(libraries_get_path('minima') . '/dist/css/minima.min.css');
    $variables['styles'] = drupal_get_css();
  }
  else {
    $style_guide_path = libraries_get_path('style_guide_' . $theme);

    $less_settings = less_get_settings($theme);
    $less_settings['variables']['minima-path'] = '~"' . libraries_get_path('minima') . '/src/less"';

    drupal_add_css($style_guide_path . '/src/less/app.less', array('less' => $less_settings));
    drupal_add_css($style_guide_path . '/src/less/ie.less', array('browsers' => array('IE' => 'lt IE 9', '!IE' => FALSE)));
    $variables['styles'] = drupal_get_css();

    drupal_add_js($style_guide_path . '/src/js/styleguide.js');
    drupal_add_js(drupal_get_path('theme', 'premium_locals') . '/js/modernizr.js');
    drupal_add_js(drupal_get_path('theme', 'premium_locals') . '/js/premium_locals.js');
    $variables['scripts'] = drupal_get_js();
  }
}

/**
 * Preprocess variables for the node template.
 */
function premium_locals_preprocess_node(&$variables) {
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];

  if ($variables['promote'] && $variables['is_front']) {
    $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__promote';
    $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'] . '__promote';
  }

  if ($variables['teaser'] && $variables['type'] == 'event') {
    $variables['content_attributes_array']['class'][] = 'Grid';
    $variables['content_attributes_array']['class'][] = 'Grid--spaceHorizontal';
  }
}

/**
 * Preprocess variables for promotion.tpl.php
 */
function premium_locals_preprocess_entity(&$variables) {
  if ($variables['elements']['#entity_type'] == 'promotion') {
    if ($hide_title_field = field_get_items('promotion', $variables['promotion'], 'field_hide_title')) {
      $variables['hide_title'] = (bool) $hide_title_field[0]['value'];
    }
  }
}

/**
 * Preprocess variables for theme minima_site_layout.
 */
function premium_locals_preprocess_minima_site_layout(&$variables) {
  // Add classes to the markup so that the page behaves responsively.
  $variables['minima_layout']['header_container']['header']['#grid_cell_attributes']['class'] = array('u-lg-size3of4', 'u-md-size3of4', 'u-sm-size3of4', 'u-xs-size3of4');
}

/**
 * Preprocess variables for theme form_element.
 */
function premium_locals_preprocess_form_element(&$variables) {
  if ($variables['element']['#type'] == 'checkbox' || $variables['element']['#type'] == 'radio') {
    $variables['element']['#title_grid_cell_attributes'] = array(
      'class' => array('u-size15of16'),
    );
    $variables['element']['#field_grid_cell_attributes'] = array(
      'class' => array('u-size1of16'),
    );
  }
  if (array_key_exists('#webform_component', $variables['element']) && is_array($variables['element']['#webform_component'])) {
    $variables['element']['#field_grid_cell_attributes'] = array(
      'class' => array('Grid-cell--formItem', 'Grid-cell--formItem' . minima_camelcase($variables['element']['#webform_component']['form_key'], '[-|_]+', true)),
    );
  }
}

/**
 * Preprocess variables for theme menu_tree.
 */
function premium_locals_preprocess_menu_tree(&$variables) {
  // Add columns classes to footer secondary menu
  if ($element_children = element_children($variables['original_tree'])) {
    if ($variables['original_tree'][reset($element_children)]['#original_link']['menu_name'] == 'menu-footer-secondary-menu') {
      $variables['attributes_array']['class'][] = 'u-xl-columns2';
      $variables['attributes_array']['class'][] = 'u-lg-columns2';
    }
  }
}

/**
 * Preprocess variables for theme panels_pane.
 */
function premium_locals_preprocess_panels_pane(&$variables) {
  switch ($variables['pane']->panel) {
    case 'navigation':
      // Display only the content for the main menu pane.
      if ($variables['pane']->subtype == 'menu_block-gk-core-main-menu') {
        $variables['theme_hook_suggestion'] = 'box__offcanvas_primary';
      }
    break;

    // Wrap boxes in the secondary region of the "page_primary" layout in cells.
    case 'secondary':
      if ($variables['display']->layout == 'pl_core_page_primary') {
        $variables['box_prefix'] = '<div class="Grid-cell u-xl-size1of3 u-lg-size1of2 u-ie-size1of3">';
        $variables['box_suffix'] = '</div>';
      }
      // Add OffCanvas JS plugin data-attribute to secondary navigation.
      elseif ($variables['pane']->type == 'navigation') {
        $variables['attributes_array']['data-offcanvas-panel'] = array('secondary');
      }
    break;

    // Wrap boxes in the footer region in cells.
    case 'footer':
      switch ($variables['pane']->subtype) {
        case 'menu_block-pl-core-footer-secondary-menu':
          $variables['box_prefix'] = '<div class="Grid-cell u-xl-size2of4 u-lg-size2of4 u-md-size1of2 u-sm-size1of2 u-ie-size2of4">';
          $variables['box_suffix'] = '</div>';
        break;

        case 'menu_block-gk-core-footer-menu':
          $variables['box_prefix'] = '<div class="Grid-cell u-xl-size1of4 u-lg-size1of4 u-md-size1of2 u-sm-size1of2 u-ie-size1of4">';
          $variables['box_suffix'] = '</div>';
        break;

        case 'gk_logo':
          $variables['box_prefix'] = '<div class="Grid-cell u-xl-size1of4 u-lg-size1of4 u-md-sizeFull u-sm-sizeFull u-ie-size1of4">';
          $variables['box_suffix'] = '</div>';
        break;
      }
    break;
  }
}

/**
 * Preprocess variables for the htmlmail template.
 */
function premium_locals_preprocess_htmlmail(&$variables) {
  // Check if there is an htmlmail suitable logo for the current domain.
  $domain = domain_get_domain();
  $domain_theme = domain_theme_lookup($domain['domain_id']);
  $domain_theme_settings = unserialize($domain_theme['settings']);

  if (!empty($domain_theme_settings['logo_path'])) {
    $htmlmail_logo_path = $domain_theme_settings['logo_path'];

    if (file_exists($htmlmail_logo_path . '.htmlmail.png')) {
      $htmlmail_logo_path .= '.htmlmail.png';
    }

    $variables['logo'] = theme('image', array(
      'path' => $htmlmail_logo_path,
    ));
  }

  // Add minima_config LESS variables to $variables to be picked up by gk_mail.
  $domain_theme_name = $domain_theme['theme'];
  $style_guide_path = libraries_get_path('style_guide_' . $domain_theme_name);

  $variables['less_variables'] = $domain_theme_settings['less'][$domain_theme_name] + array(
    'style-guide-path' => '~"' . $style_guide_path . '"',
  );
}
