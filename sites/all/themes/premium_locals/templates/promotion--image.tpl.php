<div<?php print $attributes; ?>>
  <?php if ($is_link): ?><a<?php print $link_attributes; ?>><?php endif; ?>
    <?php if (!$hide_title || $cta): ?>
      <div class="Promotion-details">
        <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
        <?php if ($cta): ?>
          <div class="Promotion-cta"><?php print $cta; ?></div>
        <?php endif; ?>
      </div>
    <?php endif; ?>
    <?php print $image; ?>
  <?php if ($is_link): ?></a><?php endif; ?>
</div>
