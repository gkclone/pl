(function($) {
  'use strict';

  Drupal.behaviors.premium_locals = {
    attach: function(context, settings) {
      $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);

          if (!target.length) {
            target = $('[name=' + this.hash.slice(1) +']');
          }

          if (target.length) {
            $('body').animate({
              scrollTop: target.offset().top
            }, 700, function() {
              if (history.pushState) {
                history.pushState(null, null, '#' + target.attr('id'));
              }
            });

            return false;
          }
        }

        return true;
      });


      // Run JS from the styleguide.
      if (typeof styleguide === 'function') {
        styleguide($);
      };
    }
  }

})(jQuery);
