<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <?php if ($display_title): ?>
      <h2 class="Node-title">
        <?php print $title; ?>
      </h2>
    <?php endif; ?>

    <div<?php print $content_attributes; ?>>
      <div class="Grid-cell u-size1of3 u-md-sizeFull u-sm-sizeFull u-xs-sizeFull">
        <?php print render($field_event_image); ?>
      </div>
      <div class="Grid-cell u-size2of3 u-md-sizeFull u-sm-sizeFull u-xs-sizeFull">
        <?php print render($field_event_date); ?>
        <?php print $body; ?>
        <?php if (!empty($links)): ?>
          <?php print $links; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php endif; ?>
