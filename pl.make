; GK Distro
includes[gk_distro] = http://code.gkdigital.co/gk_distro/v2.0.26/gk_distro.make

; MODULES ======================================================================

; Standard
projects[gk_domain][type] = module
projects[gk_domain][subdir] = standard
projects[gk_domain][download][type] = git
projects[gk_domain][download][url] = git@bitbucket.org:greeneking/gk-domain.git
projects[gk_domain][download][tag] = 7.x-1.3

projects[gk_events][type] = module
projects[gk_events][subdir] = standard
projects[gk_events][download][type] = git
projects[gk_events][download][url] = git@bitbucket.org:greeneking/gk-events.git
projects[gk_events][download][tag] = 7.x-2.6

projects[gk_locations][type] = module
projects[gk_locations][subdir] = standard
projects[gk_locations][download][type] = git
projects[gk_locations][download][url] = git@bitbucket.org:greeneking/gk-locations.git
projects[gk_locations][download][tag] = 7.x-2.6

projects[gk_members][type] = module
projects[gk_members][subdir] = standard
projects[gk_members][download][type] = git
projects[gk_members][download][url] = git@bitbucket.org:greeneking/gk-members.git
projects[gk_members][download][tag] = 7.x-2.1

projects[gk_menus][type] = module
projects[gk_menus][subdir] = standard
projects[gk_menus][download][type] = git
projects[gk_menus][download][url] = git@bitbucket.org:greeneking/gk-menus.git
projects[gk_menus][download][tag] = 7.x-2.8

projects[gk_promotions][type] = module
projects[gk_promotions][subdir] = standard
projects[gk_promotions][download][type] = git
projects[gk_promotions][download][url] = git@bitbucket.org:greeneking/gk-promotions.git
projects[gk_promotions][download][tag] = 7.x-2.7

projects[gk_rewards][type] = module
projects[gk_rewards][subdir] = standard
projects[gk_rewards][download][type] = git
projects[gk_rewards][download][url] = git@bitbucket.org:greeneking/gk-rewards.git
projects[gk_rewards][download][tag] = 7.x-2.3

projects[gk_tiers][type] = module
projects[gk_tiers][subdir] = standard
projects[gk_tiers][download][type] = git
projects[gk_tiers][download][url] = git@bitbucket.org:greeneking/gk-tiers.git
projects[gk_tiers][download][tag] = 7.x-1.3

projects[gk_vouchers][type] = module
projects[gk_vouchers][subdir] = standard
projects[gk_vouchers][download][type] = git
projects[gk_vouchers][download][url] = git@bitbucket.org:greeneking/gk-vouchers.git
projects[gk_vouchers][download][tag] = 7.x-1.6

projects[gk_webform][type] = module
projects[gk_webform][subdir] = standard
projects[gk_webform][download][type] = git
projects[gk_webform][download][url] = git@bitbucket.org:greeneking/gk-webform.git
projects[gk_webform][download][tag] = 7.x-2.3

; LIBRARIES  ===================================================================

projects[cycle2.carousel][type] = library
projects[cycle2.carousel][subdir] = ""
projects[cycle2.carousel][download][type] = file
projects[cycle2.carousel][download][url] = http://malsup.github.io/min/jquery.cycle2.carousel.min.js

projects[headroom][type] = library
projects[headroom][subdir] = ""
projects[headroom][download][type] = git
projects[headroom][download][url] = https://github.com/WickyNilliams/headroom.js.git
projects[headroom][download][tag] = v0.6.0

projects[imagesloaded][type] = library
projects[imagesloaded][subdir] = ""
projects[imagesloaded][download][type] = git
projects[imagesloaded][download][url] = https://github.com/desandro/imagesloaded.git
projects[imagesloaded][download][tag] = v3.1.8

projects[masonry][type] = library
projects[masonry][subdir] = ""
projects[masonry][download][type] = git
projects[masonry][download][url] = https://github.com/desandro/masonry.git
projects[masonry][download][tag] = v3.1.5

projects[style_guide_urban][type] = library
projects[style_guide_urban][subdir] = ""
projects[style_guide_urban][download][type] = git
projects[style_guide_urban][download][url] = git@bitbucket.org:greeneking/pl-style-guide-urban.git
projects[style_guide_urban][download][tag] = v1.0.19

projects[style_guide_urban_2][type] = library
projects[style_guide_urban_2][subdir] = ""
projects[style_guide_urban_2][download][type] = git
projects[style_guide_urban_2][download][url] = git@bitbucket.org:greeneking/pl-style-guide-urban-2.git
projects[style_guide_urban_2][download][tag] = v1.0.6

projects[style_guide_traditional][type] = library
projects[style_guide_traditional][subdir] = ""
projects[style_guide_traditional][download][type] = git
projects[style_guide_traditional][download][url] = git@bitbucket.org:greeneking/pl-style-guide-traditional.git
projects[style_guide_traditional][download][tag] = v1.0.12

projects[style_guide_traditional_2][type] = library
projects[style_guide_traditional_2][subdir] = ""
projects[style_guide_traditional_2][download][type] = git
projects[style_guide_traditional_2][download][url] = git@bitbucket.org:greeneking/pl-style-guide-traditional-2.git
projects[style_guide_traditional_2][download][tag] = v1.0.7
